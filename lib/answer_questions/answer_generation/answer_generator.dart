import 'package:magic_conche/answer_questions/answer_generation/answer_language.dart';
import 'package:magic_conche/answer_questions/answer_generation/audio_answer.dart';
import 'package:magic_conche/answer_questions/answer_generation/question.dart';
import 'package:magic_conche/answer_questions/exceptions/no_question_rules_found_exception.dart';
import 'lang/lang_rules.dart';

abstract class IAnswerGenerator {
  Future<AudioAnswer> answer(question, {AnswerLanguage language});
}

class AnswerGenerator implements IAnswerGenerator {
  final AnswerLanguage defaultLanguage;
  final List<LanguageRules> languageRules;

  AnswerGenerator(this.languageRules, this.defaultLanguage);

  Future<AudioAnswer> answer(question, {AnswerLanguage language}) async {
    return Future(() {
      final lang = language != null ? language : defaultLanguage;

      final rules = languageRules.firstWhere((x) => x.language == lang,
          orElse: () => throw new NoQuestionRulesFoundException(language));

      return answerWithRules(rules, question);
    });
  }

  AudioAnswer answerWithRules(LanguageRules rules, question) {
    final tokenizedQuestion = tokenize(question);
    final questionAnswers =
        findBestMatchinQuestionAnswers(rules.ruleSet, tokenizedQuestion);
    if (questionAnswers != null) {
      return AudioAnswer(questionAnswers.randomAnswer, rules.languageAsString);
    }
    return AudioAnswer(
        rules.defaultAnswers.randomAnswer, rules.languageAsString);
  }

  static Question findBestMatchinQuestionAnswers(
      List<Question> questionsAnswers, Iterable<String> tokenizedQuestion) {
    int maxWeight = -1;
    Question bestMatch;

    for (var i = 0; i < questionsAnswers.length; i++) {
      final questionAnswer =
          questionsAnswers[i].fullfillsAllRules(tokenizedQuestion);
      if (questionAnswer.isFullfilled && questionAnswer.weight > maxWeight) {
        maxWeight = questionAnswer.weight;
        bestMatch = questionsAnswers[i];
      }
    }
    return bestMatch;
  }

  static Iterable<String> tokenize(String sentence) {
    var tokenized = sentence
        .toLowerCase()
        .split(" ")
        .map((word) => word
            .replaceAll("?", "")
            .replaceAll("!", "")
            .replaceAll(".", "")
            .replaceAll(",", "")
            .replaceAll("oh", "")
            .replaceAll("magische", "")
            .replaceAll("miesmuschel", "")
            .replaceAll("muschel", ""))
        .where((element) => element.length > 0);
    print(tokenized);

    return tokenized;
  }
}
