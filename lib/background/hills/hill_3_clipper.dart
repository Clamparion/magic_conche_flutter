import 'package:flutter/cupertino.dart';

class Hill3Clipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = new Path();
    path.lineTo(0, size.height * 0.72);
    path.cubicTo(0, size.height * 0.72, size.width * 0.22, size.height * 0.06,
        size.width * 0.45, 0);
    path.cubicTo(size.width * 0.69, -0.05, size.width, size.height * 0.73,
        size.width, size.height * 0.73);
    path.lineTo(size.width, size.height);
    path.lineTo(0, size.height);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
