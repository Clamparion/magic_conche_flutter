import 'package:audioplayers/audio_cache.dart';
import 'package:magic_conche/answer_questions/answer_generation/answer.dart';

import '../../service_registration.dart';

class AudioAnswer {
  final Answer _textAnswer;
  final String _audioFileAsset;

  bool isPlaying = false;

  String get text => _textAnswer.answer;

  factory AudioAnswer(Answer textAnswer, String languagePrefix) {
    return AudioAnswer._(
        textAnswer, "$languagePrefix/${textAnswer.audioFileName}");
  }

  AudioAnswer._(this._textAnswer, this._audioFileAsset);

  play() {
    if (!isPlaying) {
      AudioCache player = getIt<AudioCache>();
      isPlaying = true;
      player.play(_audioFileAsset).then((audioPlayer) {
        this.isPlaying = false;
      }, onError: (error) {
        this.isPlaying = false;
        print("Error playing sound: $_audioFileAsset, $error");
      });
    }
  }
}
