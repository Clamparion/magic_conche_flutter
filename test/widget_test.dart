// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter_test/flutter_test.dart';
import 'package:magic_conche/answer_questions/answer_generation/answer_generator.dart';
import 'package:magic_conche/answer_questions/answer_generation/answer_language.dart';
import 'package:magic_conche/answer_questions/answer_generation/lang/german_lang_rules.dart';

void main() {
  testWidgets('Counter increments smoke test', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    /*await tester.pumpWidget(MyApp());

    // Verify that our counter starts at 0.
    expect(find.text('0'), findsOneWidget);
    expect(find.text('1'), findsNothing);

    // Tap the '+' icon and trigger a frame.
    await tester.tap(find.byIcon(Icons.add));
    await tester.pump();

    // Verify that our counter has incremented.
    expect(find.text('0'), findsNothing);
    expect(find.text('1'), findsOneWidget);*/
    expect(true, true);
  });

  test('It should create a german answer', () {
    final generator = AnswerGenerator([GermanLangRules()], AnswerLanguage.DE);

    final answer = generator.answer("Kann ich was zu essen haben?");

    expect(answer, "hallo");
  });
}
