import 'package:audioplayers/audio_cache.dart';
import 'package:get_it/get_it.dart';
import 'package:magic_conche/answer_questions/answer_generation/answer_language.dart';
import 'package:magic_conche/answer_questions/answer_generation/lang/german_lang_rules.dart';
import 'package:magic_conche/answer_questions/answer_question.dart';
import 'package:magic_conche/speech_to_text/speech_detection.dart';
import 'package:magic_conche/speech_to_text/speech_listener.dart';

import 'answer_questions/answer_generation/answer_generator.dart';
import 'fakes/speech_detection_fake.dart';

// This is our global ServiceLocator
final GetIt getIt = GetIt.instance;

class ServiceRegistration {
  ServiceRegistration() {
    getIt
        .registerSingleton<AudioCache>(new AudioCache(prefix: 'assets/audio/'));

    getIt.registerSingleton<ISpeechDetection>(new SpeechDetection());
    //getIt.registerSingleton<ISpeechDetection>(
    //    new SpeechDetectionFake(question: "Kann ich was zu essen haben"));

    getIt.registerSingleton<IAnswerGenerator>(
        new AnswerGenerator([GermanLangRules()], AnswerLanguage.DE));

    getIt.registerSingleton<SpeechListener>(
        new SpeechListener(getIt<ISpeechDetection>()));

    getIt.registerSingleton<IAnswerQuestion>(new AnswerQuestion(
        getIt<ISpeechDetection>(), getIt<IAnswerGenerator>()));
  }
}
