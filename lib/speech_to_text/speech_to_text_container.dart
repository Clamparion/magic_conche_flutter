import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:magic_conche/animations/jumping_dots.dart';
import 'package:magic_conche/conch_colors.dart';
import 'package:magic_conche/speech_to_text/speech_detection.dart';
import 'package:magic_conche/speech_to_text/speech_floating_button.dart';
import '../service_registration.dart';

class SpeechToTextContainer extends StatelessWidget {
  const SpeechToTextContainer({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: StreamBuilder(
          stream: getIt<ISpeechDetection>().stream$,
          builder: (context, snapshot) {
            final data = snapshot.data;
            final isListening = data?.isListening ?? false;
            final text = data?.text ?? "";
            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SpeechFloatingButton(isListening: isListening),
                text.length > 0
                    ? Container(
                        padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                        child: Text(
                          data.text,
                          style: TextStyle(
                              fontFamily: 'SnapHand',
                              color: ConchColors.questionTextColor,
                              fontSize: 32),
                          textAlign: TextAlign.center,
                        ),
                      )
                    : (isListening
                        ? Transform.translate(
                            offset: Offset(0, -30),
                            child: JumpingDotsProgressIndicator(
                              numberOfDots: 3,
                              fontSize: 50.0,
                              color: ConchColors.questionTextColor,
                            ),
                          )
                        : Container()),
              ],
            );
          }),
    );
  }
}
