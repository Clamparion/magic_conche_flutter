import 'package:magic_conche/answer_questions/answer_generation/answer_language.dart';

class NoQuestionRulesFoundException implements Exception {
  String msg;

  factory NoQuestionRulesFoundException(AnswerLanguage language) =>
      NoQuestionRulesFoundException._(
          "There is no AnswerHandler for this specific language defined $language");

  NoQuestionRulesFoundException._(this.msg);

  String toString() => 'NoQuestionRulesFoundException: $msg';
}
