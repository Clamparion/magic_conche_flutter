import 'dart:async';

import 'package:magic_conche/answer_questions/answer_generation/answer_generator.dart';
import 'package:magic_conche/answer_questions/answer_generation/audio_answer.dart';
import 'package:magic_conche/speech_to_text/speech_detection.dart';
import 'package:rxdart/rxdart.dart';

class AnswerQuestionState {
  final String question;
  final AudioAnswer answer;
  final bool isAnswered;
  AnswerQuestionState(this.isAnswered, {this.answer, this.question = ""});
}

abstract class IAnswerQuestion {
  Stream<AnswerQuestionState> get stream$;
}

class AnswerQuestion implements IAnswerQuestion {
  final ISpeechDetection speechDetection;
  final IAnswerGenerator answerGenerator;
  PublishSubject<AnswerQuestionState> _answerQuestionSubject =
      PublishSubject<AnswerQuestionState>();

  AnswerQuestion(this.speechDetection, this.answerGenerator) {
    speechDetection.stream$
        .where(_isJustStartedOrFinishedListening)
        .listen((speechDetectionState) async {
      var answerToQuestion = await _generateAnswer(speechDetectionState);
      _answerQuestionSubject.add(answerToQuestion);
    });
  }

  Stream<AnswerQuestionState> get stream$ => _answerQuestionSubject;

  bool _isJustStartedOrFinishedListening(SpeechDetectionState streamData) {
    final justFinishedListeing = streamData?.justFinishedListeing ?? false;
    final justStartedListeing = streamData?.justStartedListeing ?? false;
    return (justStartedListeing || justFinishedListeing);
  }

  Future<AnswerQuestionState> _generateAnswer(
      SpeechDetectionState speechDetectionState) async {
    final justFinishedListeing =
        speechDetectionState?.justFinishedListeing ?? false;
    final question = speechDetectionState.text ?? "";
    if (question.length > 0 && justFinishedListeing) {
      final answer = await answerGenerator.answer(question);
      return AnswerQuestionState(true, question: question, answer: answer);
    }
    return (AnswerQuestionState(false));
  }
}
