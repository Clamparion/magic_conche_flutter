import 'package:flutter/widgets.dart';

import '../conch_colors.dart';
import '../service_registration.dart';
import 'answer_question.dart';

class AnswerQuestionContainer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: StreamBuilder(
            stream: getIt<IAnswerQuestion>().stream$,
            builder: (context, snapshot) {
              if (snapshot.hasError) {
                print(snapshot.error);
                return Container();
              }

              final answerQuestionState = snapshot.data as AnswerQuestionState;
              final answerText = answerQuestionState?.answer?.text ?? "";

              answerQuestionState?.answer?.play();

              final question = answerQuestionState?.question ?? "";
              if (answerText.length > 0) {
                return Text(
                  answerText,
                  style: TextStyle(
                      fontFamily: 'SnapHand',
                      color: ConchColors.questionTextColor,
                      fontSize: 32),
                  textAlign: TextAlign.center,
                );
              } else {
                return Container();
              }
            }));
  }
}
