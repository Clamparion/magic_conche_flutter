import 'package:flutter/cupertino.dart';

class Hill1Clipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = new Path();
    path.lineTo(0, size.height);
    path.lineTo(0, size.height * 0.42);

    path.cubicTo(size.width * 0.42, size.height * 0.8, size.width * 0.6, 0,
        size.width, 0);
    path.lineTo(size.width, size.height);
    path.lineTo(0, size.height);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
