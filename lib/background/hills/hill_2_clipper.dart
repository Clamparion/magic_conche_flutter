import 'package:flutter/cupertino.dart';

class Hill2Clipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = new Path();
    path.lineTo(0, size.height * 0.18);
    path.cubicTo(0, size.height * 0.18, size.width * 0.07, size.height * 0.11,
        size.width * 0.16, size.height * 0.27);
    path.cubicTo(size.width * 0.28, size.height * 0.46, size.width * 0.3,
        size.height * 0.72, size.width * 0.44, size.height * 0.71);
    path.cubicTo(size.width * 0.55, size.height * 0.71, size.width * 0.75,
        size.height * 0.02, size.width * 0.87, 0);
    path.cubicTo(size.width * 0.95, 0, size.width, size.height * 0.16,
        size.width, size.height * 0.16);
    path.lineTo(size.width, size.height);
    path.lineTo(0, size.height);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
