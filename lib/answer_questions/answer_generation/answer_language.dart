enum AnswerLanguage { DE, EN }

extension ParseToString on AnswerLanguage {
  String toShortString() {
    return this.toString().split('.').last;
  }
}
