import 'package:flutter/material.dart';
import 'package:magic_conche/extensions/hex_color.dart';

class ConchColors {
  static const CordColor = Colors.white;
  static const CordBorderColor = Colors.grey;

  static final backgroundGradientStartColor = HexColor("#42b8d5");
  static final backgroundGradientEndColor = HexColor("#17527c");

  static final hill1GradientStartColor = HexColor("#719e37");
  static final hill1GradientEndColor = HexColor("#537a2c");

  static final hill2GradientStartColor = HexColor("#6e9d33");
  static final hill2GradientEndColor = HexColor("#395c1e");

  static final hill3GradientStartColor = HexColor("#548b2d");
  static final hill3GradientEndColor = HexColor("#27460e");

  static final rayGradientStartColor = HexColor("#78e6f0").withAlpha(200);
  static final rayGradientEndColor = Colors.white.withAlpha(0);

  static final micButtonBackgroundColor = Colors.white;
  static final micButtonIconColor = HexColor("#42b8d5");

  static final questionTextColor = Colors.white;

  static final starColors = [
    HexColor("2893b7"),
    HexColor("4ca7ba"),
    HexColor("69a599"),
    HexColor("58cca7"),
    HexColor("4da3ba"),
    HexColor("6d818c")
  ];
}
