import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import '../../conch_colors.dart';

class Star extends StatelessWidget {
  final int index;
  final double size;
  final Offset offset;

  Star(this.index, {this.size = 150, this.offset = Offset.zero}) {}

  @override
  Widget build(BuildContext context) {
    final Widget image = Image.asset(
      "assets/images/star${index + 1}.png",
      fit: BoxFit.contain,
      color: ConchColors.starColors[index],
    );

    return Positioned(
      top: this.offset.dy,
      left: this.offset.dx,
      child: SizedBox(width: size, height: size, child: image),
    );
  }
}
