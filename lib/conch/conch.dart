import 'package:flutter/widgets.dart';
import 'package:flutter_svg/svg.dart';
import 'package:magic_conche/conch/bubbles/bubbles.dart';
import 'conch_cord.dart';

class Conch extends StatelessWidget {
  final double posX;
  final double posY;
  const Conch({Key key, this.posX, this.posY}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final String assetName = 'assets/svgs/conch.svg';
    final Widget svg = SvgPicture.asset(assetName, fit: BoxFit.contain);

    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;

    return Stack(
      children: [
        Center(
          child: Container(
              width: 180,
              child: Transform.translate(offset: Offset(-27, -52), child: svg)),
        ),
        Bubbles(),
        Container(
            width: screenWidth,
            height: screenHeight,
            child: ConchCord(posX, posY)),
      ],
    );
  }
}
