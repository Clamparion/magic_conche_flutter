import 'package:avatar_glow/avatar_glow.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:magic_conche/conch_colors.dart';

class SpeechFloatingButton extends StatelessWidget {
  final bool isListening;

  const SpeechFloatingButton({Key key, this.isListening = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: AnimatedOpacity(
        duration: const Duration(milliseconds: 300),
        opacity: isListening ? 1.0 : 0.0,
        child: AvatarGlow(
          animate: isListening,
          glowColor: ConchColors.micButtonBackgroundColor,
          endRadius: 50.0,
          duration: const Duration(milliseconds: 1500),
          repeatPauseDuration: const Duration(milliseconds: 100),
          repeat: true,
          startDelay: const Duration(milliseconds: 0),
          child: FloatingActionButton(
            elevation: 0,
            onPressed: () {},
            backgroundColor: ConchColors.micButtonBackgroundColor,
            foregroundColor: ConchColors.micButtonIconColor,
            child: Icon(Icons.mic),
          ),
        ),
      ),
    );
  }
}
