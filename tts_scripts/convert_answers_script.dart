import 'dart:io';

import 'package:magic_conche/answer_questions/answer_generation/answer.dart';
import 'package:magic_conche/answer_questions/answer_generation/answer_language.dart';
import 'package:magic_conche/answer_questions/answer_generation/lang/german_lang_rules.dart';
import 'package:magic_conche/answer_questions/answer_generation/lang/lang_rules.dart';

import 'classes/azure_tts.dart';
import 'classes/voice_config.dart';

Future<void> main() async {
  String subscitionKey =
      await File('${Directory.current.path}/.azure_tts_subscription_key')
          .readAsString();

  var azureTTS = TTSAzure(subscitionKey, 'eastus');
  final rules = GermanLangRules();

  answersForRules(azureTTS, rules);
}

Future<void> answersForRules(
    TTSAzure azureTTS, LanguageRules languageRules) async {
  final VoiceConfig voiceConfig = getConfigForLanguage(languageRules.language);

  final String assetFolder =
      "${Directory.current.path}/assets/audio/${languageRules.languageAsString}";

  for (var rules in languageRules.ruleSet) {
    await generateAudioAnswers(
        azureTTS, voiceConfig, rules.answers, assetFolder);
  }

  await generateAudioAnswers(
      azureTTS, voiceConfig, languageRules.defaultAnswers.answers, assetFolder);
}

Future<void> generateAudioAnswers(TTSAzure azureTTS, VoiceConfig voiceConfig,
    List<Answer> answers, String outputFolder) async {
  for (var answer in answers) {
    final String audioFile = "$outputFolder/${answer.audioFileName}";
    if (FileSystemEntity.typeSync(audioFile) == FileSystemEntityType.notFound) {
      await azureTTS.generate(answer.pronunciacion, voiceConfig.lang,
          voiceConfig.shortName, audioFile);
    }
  }
}

VoiceConfig getConfigForLanguage(AnswerLanguage language) {
  switch (language) {
    case AnswerLanguage.DE:
      return VoiceConfig("de-DE", "de-DE-KatjaNeural");
    case AnswerLanguage.EN:
      return VoiceConfig("en-US", "de-DE-KatjaNeural");
    default:
      return VoiceConfig("de-DE", "de-DE-KatjaNeural");
  }
}
