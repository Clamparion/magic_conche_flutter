import 'package:flutter/widgets.dart';
import 'dart:math';

typedef VoidCallback = void Function();

class Bubble extends StatefulWidget {
  final double width;
  final double height;
  final AnimationController controller;
  final VoidCallback onBubbleFinished;

  const Bubble(this.controller, this.width, this.height, this.onBubbleFinished,
      {Key key})
      : super(key: key);

  @override
  _BubbleState createState() => _BubbleState(
      this.controller, this.width, this.height, this.onBubbleFinished);
}

class _BubbleState extends State<Bubble> with SingleTickerProviderStateMixin {
  final double width;
  final double height;
  final VoidCallback onBubbleFinished;
  final AnimationController _controller;
  Random _rnd;

  Animation<double> _animationFloatUp;
  Animation<double> _animationFloatLeftRight;
  Animation<double> _animationSize;
  Animation<double> _animationOpacity;

  Offset _bubbleLocation = Offset(0, 0);
  double _bubbleStartSize = 2;
  double _bubbleEndSize = 50;

  double _bubbleEndXLocation = 0;

  double _bubbleSize = 0;
  double _opacity = 1;

  double _randProgressMulitplicator = 1.0;

  _BubbleState(
      this._controller, this.width, this.height, this.onBubbleFinished) {
    _rnd = Random();
    _bubbleStartSize = random((width / 150).floor(), (width / 80).floor());
    _bubbleEndSize = random((width / 10).floor(), (width / 7).floor());
    _bubbleEndXLocation = random(-(width / 3).floor(), (width / 3).floor());
    _randProgressMulitplicator = random(8, 12) / 10;
  }

  @override
  void initState() {
    super.initState();
    startBubbleAnimation();
  }

  random(int min, int max) {
    return (min + _rnd.nextInt(max - min)) * _rnd.nextDouble();
  }

  startBubbleAnimation() {
    _animationFloatUp = Tween<double>(begin: 0, end: -height / 2)
        .animate(CurvedAnimation(parent: _controller, curve: Curves.easeIn));
    _animationSize = Tween<double>(begin: _bubbleStartSize, end: _bubbleEndSize)
        .animate(
            CurvedAnimation(parent: _controller, curve: Curves.easeOutBack));
    _animationFloatLeftRight = Tween<double>(begin: 0, end: _bubbleEndXLocation)
        .animate(
            CurvedAnimation(parent: _controller, curve: Curves.decelerate));
    _animationOpacity = Tween<double>(begin: 1, end: 0).animate(
        CurvedAnimation(parent: _controller, curve: Curves.easeInExpo));

    _controller.addListener(this.animationUpdated);
  }

  void animationUpdated() {
    setState(() {
      this._bubbleLocation = Offset(
          _animationFloatLeftRight.value * _randProgressMulitplicator,
          _animationFloatUp.value * _randProgressMulitplicator);
      this._bubbleSize = _animationSize.value * _randProgressMulitplicator;
      this._opacity = _animationOpacity.value;
    });
    if (_animationOpacity.value == 0) {
      this.onBubbleFinished();
    }
  }

  @override
  void dispose() {
    _controller.removeListener(this.animationUpdated);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Transform.translate(
            offset: _bubbleLocation,
            child: Image.asset('assets/images/bubble.png',
                height: _bubbleSize,
                width: _bubbleSize,
                fit: BoxFit.fill,
                color: Color.fromRGBO(255, 255, 255, _opacity),
                colorBlendMode: BlendMode.modulate)));
  }
}
