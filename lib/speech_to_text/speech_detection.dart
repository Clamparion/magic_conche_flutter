import 'package:rxdart/rxdart.dart';
import '../extensions/string_extensions.dart';

class SpeechDetectionError implements Exception {
  final String message;
  final bool permanent;
  SpeechDetectionError(this.message, {this.permanent = false});
}

class SpeechDetectionState {
  final String text;
  final bool isListening;
  final bool justFinishedListeing;
  final bool justStartedListeing;
  SpeechDetectionState(this.text, this.isListening,
      {this.justFinishedListeing = false, this.justStartedListeing = false});
}

abstract class ISpeechDetection {
  setText(String newText);
  setIsListening(bool isListening);
  setError(SpeechDetectionError error);
  ValueStream<SpeechDetectionState> get stream$;
  SpeechDetectionState get current;
}

class SpeechDetection implements ISpeechDetection {
  BehaviorSubject<SpeechDetectionState> _speechToText =
      BehaviorSubject<SpeechDetectionState>.seeded(
          SpeechDetectionState("", false));

  ValueStream<SpeechDetectionState> get stream$ => _speechToText.stream;
  SpeechDetectionState get current => _speechToText.value;

  setText(String newText) {
    _speechToText.add(SpeechDetectionState(
        _getTextAsQuestion(newText), current?.isListening ?? false));
  }

  setIsListening(bool isListening) {
    final wasListeningBefore = (current?.isListening ?? false);
    _speechToText.add(SpeechDetectionState(current?.text ?? "", isListening,
        justFinishedListeing: wasListeningBefore && !isListening,
        justStartedListeing: !wasListeningBefore && isListening));
  }

  setError(SpeechDetectionError error) {
    _speechToText.addError(error);
  }

  String _getTextAsQuestion(String text) {
    return text.capitalize() +
        ((text.length > 0)
            ? (!(current?.isListening ?? false) ? "?" : "..")
            : "");
  }
}
