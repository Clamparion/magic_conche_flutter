import 'package:magic_conche/speech_to_text/speech_detection.dart';
import 'package:rxdart/rxdart.dart';

class SpeechDetectionFake implements ISpeechDetection {
  final String question;

  BehaviorSubject<SpeechDetectionState> _speechToText =
      BehaviorSubject<SpeechDetectionState>.seeded(
          SpeechDetectionState("", false));

  ValueStream<SpeechDetectionState> get stream$ => _speechToText.stream;
  SpeechDetectionState get current => _speechToText.value;

  SpeechDetectionFake({this.question = "Kann ich was zu essen haben?"}) {
    Future.delayed(const Duration(milliseconds: 3000), () {
      setFakeText();
    });
  }

  setFakeText() {
    final words = question.split(' ');
    _speechToText
        .add(SpeechDetectionState("", true, justStartedListeing: true));
    for (var i = 0; i < words.length; i++) {
      final text = words.getRange(0, i + 1).join(" ");
      Future.delayed(Duration(milliseconds: 200 * i), () {
        _speechToText.add(SpeechDetectionState(text, true));
      });
      if (i == words.length - 1) {
        Future.delayed(Duration(milliseconds: 200 * (i + 1)), () {
          _speechToText.add(
              SpeechDetectionState(text, false, justFinishedListeing: true));
        });
      }
    }
  }

  setText(String newText) {}

  setIsListening(bool isListening) {}

  setError(SpeechDetectionError error) {}
}
