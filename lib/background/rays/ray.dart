import 'package:flutter/widgets.dart';

class Ray extends StatelessWidget {
  final Size _size;
  final double slope;
  final double angle;
  final Offset origin;
  final double opacity;

  Ray(this._size,
      {this.slope = 0.15,
      this.angle = 0,
      this.origin = Offset.zero,
      this.opacity = 1});

  @override
  Widget build(BuildContext context) {
    return Transform.translate(
      offset: Offset(this.origin.dx - this._size.width / 2, this.origin.dy),
      child: Transform.rotate(
        origin: Offset(this._size.width / 2, -this._size.height / 2),
        angle: this.angle,
        child: Opacity(
          opacity: opacity,
          child: Image.asset(
            'assets/images/ray.png',
            height: this._size.height,
            width: this._size.width,
            fit: BoxFit.fill,
          ),
        ),
      ),
    );
  }
}

/*
CustomPaint(
            size: Size(this._size.width, this._size.height),
            painter: RayPainter(
                slope, _gradientStartColor, _gradientEndColor, alpha))
*/
