import 'package:magic_conche/speech_to_text/speech_detection.dart';
import 'package:speech_to_text/speech_to_text.dart';

class SpeechListener {
  final SpeechToText _speech = SpeechToText();
  bool isListening = false;

  void stop() {
    _speech.stop();
  }

  final ISpeechDetection _speechDetection;
  SpeechListener(this._speechDetection);

  void startListening(bool available) {
    if (available && isListening) {
      _speech.listen(
          listenMode: ListenMode.search,
          listenFor: const Duration(seconds: 90),
          pauseFor: const Duration(seconds: 60),
          onResult: (val) => _speechDetection.setText(val.recognizedWords));
    } else {
      emitStopped();
    }
  }

  emitStopped() {
    isListening = false;
    _speechDetection.setIsListening(false);
  }

  emitOnError(SpeechDetectionError error) {
    _speechDetection.setIsListening(false);
    _speechDetection.setError(error);
    isListening = false;
  }

  listen() {
    if (!isListening) {
      _speechDetection.setText("");
      _speechDetection.setIsListening(true);
      isListening = true;
      _speech
          .initialize(
            onStatus: (val) => {
              if (val == "notListening") {emitStopped()}
            },
            onError: (val) => emitOnError(SpeechDetectionError(val.errorMsg)),
          )
          .then((available) => startListening(available));
    }
  }
}
