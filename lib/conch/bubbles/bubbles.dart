import 'dart:async';

import 'package:audioplayers/audio_cache.dart';
import 'package:flutter/widgets.dart';
import 'package:magic_conche/answer_questions/answer_question.dart';

import '../../service_registration.dart';
import 'bubble.dart';

class Bubbles extends StatefulWidget {
  final numberOfBubbles;

  const Bubbles({Key key, this.numberOfBubbles = 15}) : super(key: key);

  @override
  _BubblesState createState() => _BubblesState(numberOfBubbles);
}

class _BubblesState extends State<Bubbles> with SingleTickerProviderStateMixin {
  final numberOfBubbles;
  final bubbleSoundFile = "bubbleSound.ogg";

  bool _bubblesAreDisplayed = false;
  int _numberOfFinishedBubbles = 0;
  AnimationController controller;

  StreamSubscription<AnswerQuestionState> streamSubscription;

  _BubblesState(this.numberOfBubbles) {
    listenToAnswerResults();
    controller = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 2000));
  }

  @override
  void initState() {
    super.initState();

    AudioCache audioCache = getIt<AudioCache>();
    audioCache.load(bubbleSoundFile).then(
        (value) => print("Bubble Sound loaded"),
        onError: (err) => print("Error loading Bubble sound! $err"));
  }

  void listenToAnswerResults() {
    streamSubscription =
        getIt<IAnswerQuestion>().stream$.listen((answerQuestionState) {
      final answerText = answerQuestionState?.answer?.text ?? "";
      if (answerText.length > 0) {
        setState(() {
          this._numberOfFinishedBubbles = 0;
          this._bubblesAreDisplayed = true;
        });
        this.controller.reset();
        this.controller.forward();
        this.playBubbleSound();
      }
    });
  }

  void playBubbleSound() {
    AudioCache audioCache = getIt<AudioCache>();
    audioCache.play(bubbleSoundFile, volume: 0.7);
  }

  void bubbleFinishedCallback() {
    _numberOfFinishedBubbles += 1;
    if (_numberOfFinishedBubbles == numberOfBubbles) {
      setState(() {
        this._bubblesAreDisplayed = false;
      });
    }
  }

  @override
  void dispose() {
    controller.dispose();
    streamSubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;

    if (_bubblesAreDisplayed) {
      var bubbles = new List<Widget>();
      for (var i = 0; i < this.numberOfBubbles; i++) {
        bubbles.add(Transform.translate(
            offset: Offset((screenWidth / 2) - 40, (screenHeight / 2) - 52),
            child: Bubble(this.controller, screenWidth, screenHeight,
                bubbleFinishedCallback)));
      }
      return Container(child: Stack(children: bubbles));
    } else {
      return Container();
    }
  }
}
