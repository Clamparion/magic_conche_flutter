import 'package:flutter/material.dart';
import 'package:magic_conche/answer_questions/answer_questions_container.dart';
import 'package:magic_conche/background/conch_background.dart';
import 'package:magic_conche/service_registration.dart';
import 'conch/conch.dart';
import 'speech_to_text/speech_to_text_container.dart';

void main() {
  ServiceRegistration();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      showPerformanceOverlay: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'The Magic Conch'),
    );
  }
}

class MyHomePage extends StatelessWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;

    double middleX = screenWidth / 2;
    double middleY = screenHeight / 2;

    return Scaffold(
        body: Center(
            // Center is a layout widget. It takes a single child and positions it
            // in the middle of the parent.
            child: Stack(children: [
      ConchBackground(),
      Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(
              width: screenWidth,
              height: screenHeight / 3,
              child: SpeechToTextContainer()),
          SizedBox(
              width: screenWidth,
              height: screenHeight / 3,
              child: AnswerQuestionContainer()),
        ],
      ),
      LayoutBuilder(
          builder: (context, constraints) => Container(
              width: constraints.widthConstraints().maxWidth,
              height: constraints.heightConstraints().maxHeight,
              child: Conch(posX: middleX, posY: middleY)))
    ])));
  }
}
