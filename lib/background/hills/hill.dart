import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:magic_conche/conch_colors.dart';

class Hill extends StatelessWidget {
  final double _height;
  final CustomClipper<Path> _clipper;
  final Color gradientStartColor;
  final Color gradientEndColor;

  Hill(this._height, this._clipper,
      {this.gradientStartColor, this.gradientEndColor});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Container(
          child: ClipPath(
            child: Container(
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [
                    this.gradientStartColor ??
                        ConchColors.hill1GradientStartColor,
                    this.gradientEndColor ?? ConchColors.hill1GradientEndColor
                  ])),
              width: double.infinity,
              height: this._height,
            ),
            clipper: this._clipper,
          ),
        ),
      ],
    );
  }
}
