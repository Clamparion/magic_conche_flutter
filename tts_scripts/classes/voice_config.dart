class VoiceConfig {
  final String lang;
  final String shortName;

  VoiceConfig(this.lang, this.shortName);
}
