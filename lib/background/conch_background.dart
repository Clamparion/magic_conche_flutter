import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:magic_conche/background/hills/hills.dart';
import 'package:magic_conche/background/rays/rays.dart';
import 'package:magic_conche/background/stars/stars.dart';
import 'package:magic_conche/conch_colors.dart';

class ConchBackground extends StatefulWidget {
  @override
  _ConchBackgroundState createState() => _ConchBackgroundState();
}

class _ConchBackgroundState extends State<ConchBackground> {
  @override
  Widget build(BuildContext context) {
    return ClipRect(
      child: LayoutBuilder(builder: (context, constraints) {
        final w = constraints.widthConstraints().maxWidth;
        final h = constraints.heightConstraints().maxHeight;
        return Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                  ConchColors.backgroundGradientStartColor,
                  ConchColors.backgroundGradientEndColor
                ])),
            width: w,
            height: h,
            child: Stack(children: [
              Rays(),
              //blur(w, h, 8),
              Stars(),
              //blur(w, h, 2),
              Hills()
            ]));
      }),
    );
  }

  BackdropFilter blur(double width, double height, double amount) {
    return BackdropFilter(
        filter: ImageFilter.blur(sigmaX: amount, sigmaY: amount),
        child: Container(
          width: width,
          height: height,
          color: Colors.red.withAlpha(0),
        ));
  }

  double width(BuildContext context) {
    return MediaQuery.of(context).size.width;
  }

  double height(BuildContext context) {
    return MediaQuery.of(context).size.height;
  }
}
