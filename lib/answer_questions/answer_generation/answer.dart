class Answer {
  final String answer;
  final double randomWeight;
  final String pronunciacion;

  final String audioFileName;

  static const String audioFileFormat = "mp3";

  static String _getFileName(String answer) {
    String fileName =
        answer.toLowerCase().trim().replaceAll('  ', ' ').replaceAll(' ', '_');
    fileName = fileName.replaceAll('ä', 'ae');
    fileName = fileName.replaceAll('ö', 'oe');
    fileName = fileName.replaceAll('ü', 'ue');
    fileName = fileName.replaceAll('ß', 'ss');
    return "$fileName.${Answer.audioFileFormat}";
  }

  factory Answer(String answer,
          {String pronunciacion = "", double randomWeight = 1.0}) =>
      Answer._(answer, pronunciacion.isNotEmpty ? pronunciacion : answer,
          _getFileName(answer),
          randomWeight: randomWeight);

  Answer._(this.answer, this.pronunciacion, this.audioFileName,
      {this.randomWeight = 1.0});
}
