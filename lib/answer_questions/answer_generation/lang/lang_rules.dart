import 'package:magic_conche/answer_questions/answer_generation/question.dart';
import '../answer_language.dart';

abstract class LanguageRules {
  List<Question> get ruleSet;
  Question get defaultAnswers;
  AnswerLanguage get language;

  String get languageAsString;
}
