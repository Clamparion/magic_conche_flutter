import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:magic_conche/speech_to_text/speech_listener.dart';
import 'package:magic_conche/speech_to_text/speech_detection.dart';
import 'package:vector_math/vector_math.dart';
import 'conch_cord_painter.dart';
import '../service_registration.dart';

class ConchCord extends StatefulWidget {
  final double anchorX;
  final double anchorY;

  ConchCord(this.anchorX, this.anchorY);

  @override
  ConchCordState createState() => ConchCordState(anchorX, anchorY);
}

class ConchCordState extends State<ConchCord>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  Animation<double> animation;

  StreamSubscription speechDetectionSubscription;

  ConchCordPainter _conchPainter;
  final double triggerLimitValue = 4000;
  final double dragStartLimitValue = 3000;
  final Vector2 anchor;
  SpeechListener _speechListener;

  Vector2 lockedPos;
  factory ConchCordState(double anchorX, double anchorY) {
    return ConchCordState._(Vector2(anchorX, anchorY));
  }

  ConchCordState._(this.anchor) {
    _conchPainter = ConchCordPainter(anchor);
    _speechListener = getIt<SpeechListener>();
  }
  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 400));

    speechDetectionSubscription =
        getIt<ISpeechDetection>().stream$.listen((event) {
      if (event?.justFinishedListeing ?? false) {
        startLerpBack();
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
    speechDetectionSubscription.cancel();
  }

  bool _isTriggered() {
    final sqLenVec = _conchPainter.pos.distanceToSquared(_conchPainter.anchor);
    return sqLenVec > this.triggerLimitValue;
  }

  bool _isDragStartNearAnchor(double x, double y) {
    final sqLenVec = Vector2(x, y).distanceToSquared(_conchPainter.anchor);
    return sqLenVec < this.dragStartLimitValue;
  }

  startLerpBackAnimation() {
    _controller.reset();
    final Animation curve =
        CurvedAnimation(parent: _controller, curve: Curves.easeOutCubic);
    animation = Tween<double>(begin: 1, end: 0).animate(curve);
    animation.addListener(() {
      setState(() {
        _conchPainter = _conchPainter.copyWith(
            newPos: _conchPainter.anchor +
                (lockedPos - _conchPainter.anchor) * animation.value);
      });
    });
    _controller.forward();
  }

  _dragStart(DragStartDetails details) {
    setState(() {
      final x = details.localPosition.dx;
      final y = details.localPosition.dy;
      if (_isDragStartNearAnchor(x, y)) {
        _conchPainter =
            _conchPainter.copyWith(newPos: Vector2(x, y), newIsDragging: true);
      }
    });
  }

  _dragUpdate(DragUpdateDetails details) {
    if (_conchPainter.isDragging) {
      setState(() {
        _conchPainter = _conchPainter.copyWith(
            newPos:
                Vector2(details.localPosition.dx, details.localPosition.dy));
        if (!(getIt<ISpeechDetection>().current?.isListening ?? false) &&
            _isTriggered()) {
          _listen();
        }
      });
    }
  }

  _dragEnd(DragEndDetails details) {
    _stopListen();
  }

  void startLerpBack() {
    setState(() {
      _conchPainter = _conchPainter.copyWith(newIsDragging: false);
      lockedPos = _conchPainter.pos.clone();
    });
    startLerpBackAnimation();
  }

  void _stopListen() {
    startLerpBack();
    _speechListener.stop();
  }

  void _listen() {
    _speechListener.listen();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: GestureDetector(
            onHorizontalDragStart: _dragStart,
            onVerticalDragStart: _dragStart,
            onHorizontalDragUpdate: _dragUpdate,
            onVerticalDragUpdate: _dragUpdate,
            onHorizontalDragEnd: _dragEnd,
            onVerticalDragEnd: _dragEnd,
            child: Container(child: CustomPaint(painter: _conchPainter))));
  }
}
