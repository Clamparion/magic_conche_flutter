import "package:magic_conche/answer_questions/answer_generation/answer.dart";
import "package:magic_conche/answer_questions/answer_generation/answer_language.dart";
import "package:magic_conche/answer_questions/answer_generation/lang/lang_rules.dart";
import "package:magic_conche/answer_questions/answer_generation/question.dart";
import "package:magic_conche/answer_questions/answer_generation/question_rule.dart";

class GermanLangRules implements LanguageRules {
  @override
  AnswerLanguage get language => AnswerLanguage.DE;

  @override
  String get languageAsString => 'de';

  final List<Question> finalRuleSet = [
    Question(tag: "german_wer_question")
        .addRule(ContainsQuestionWord("wer"))
        .addAnswer(Answer("Batman"))
        .addAnswer(Answer("Der Weihnachtsmann"))
        .addAnswer(Answer("Spongebob Schwammkopf"))
        .addAnswer(Answer("Patrick Star"))
        .addAnswer(Answer("Thaddäus"))
        .addAnswer(Answer("Homer Simpson"))
        .addAnswer(Answer("Dein Vater"))
        .addAnswer(Answer("Deine Großeltern"))
        .addAnswer(Answer("Dein Nachbar"))
        .addAnswer(Answer("Deine Nachbarin"))
        .addAnswer(Answer("Dein bester Freund"))
        .addAnswer(Answer("Deine beste Freundin"))
        .addAnswer(Answer("Capital Bra", pronunciacion: "Kapital Brah"))
        .addAnswer(Answer("187 Strassenbande",
            pronunciacion: "Eins Acht Sieben Strassenbande"))
        .addAnswer(Answer("Mero"))
        .addAnswer(Answer("Loredana"))
        .addAnswer(Answer("Captain America", pronunciacion: "Kepten Amerika"))
        .addAnswer(Answer("Iron Man", pronunciacion: "Eiren Män"))
        .addAnswer(Answer("Annegret Kramp-Karrenbauer"))
        .addAnswer(Answer("Deine Oma"))
        .addAnswer(Answer("Dein Opa"))
        .addAnswer(Answer("Simon Unge"))
        .addAnswer(Answer("Deine Mutter")),
    Question(tag: "german_wie_question")
        .addRule(ContainsQuestionWord("wie"))
        .addAnswer(Answer("Mit Honig"))
        .addAnswer(Answer("Mit Geld"))
        .addAnswer(Answer("Mit Wissen"))
        .addAnswer(Answer("Keine Ahnung"))
        .addAnswer(Answer("Mit Geduld"))
        .addAnswer(Answer("Mit Soße"))
        .addAnswer(Answer("Mithilfe eines Protonenbeschleunigers"))
        .addAnswer(Answer("Mithilfe deines Verstandes"))
        .addAnswer(Answer("Mithilfe von deinen Eltern"))
        .addAnswer(Answer("Mithilfe deiner Hand"))
        .addAnswer(Answer("Mithilfe von Quantenverschränkung"))
        .addAnswer(Answer("Mithilfe von Krautsalat"))
        .addAnswer(Answer("Frag mich was Leichteres"))
        .addAnswer(Answer("So wie du willst", randomWeight: 2)),
    Question(tag: "german_was_question")
        .addRule(ContainsQuestionWord("was"))
        .addAnswer(Answer("Garnichts"))
        .addAnswer(Answer("Alles"))
        .addAnswer(Answer("Einiges"))
        .addAnswer(Answer("So manches")),
    Question(tag: "german_wo_question")
        .addRule(ContainsQuestionWord("wo"))
        .addAnswer(Answer("Such dir einen Ort aus"))
        .addAnswer(Answer("Lateinamerika"))
        .addAnswer(Answer("Amerika"))
        .addAnswer(Answer("China"))
        .addAnswer(Answer("Deutschland"))
        .addAnswer(Answer("Frankreich"))
        .addAnswer(Answer("Venezuela"))
        .addAnswer(Answer("Italien"))
        .addAnswer(Answer("Polen"))
        .addAnswer(Answer("Spanien"))
        .addAnswer(Answer("England"))
        .addAnswer(Answer("Russland"))
        .addAnswer(Answer("Türkei"))
        .addAnswer(Answer("Griechenland"))
        .addAnswer(Answer("Holland"))
        .addAnswer(Answer("Schweden"))
        .addAnswer(Answer("Afrika")),
    Question(tag: "german_wann_question")
        .addRule(ContainsQuestionWord("wann"))
        .addAnswer(Answer("Irgendwann"))
        .addAnswer(Answer("Später"))
        .addAnswer(Answer("Morgen"))
        .addAnswer(Answer("In drei Tagen"))
        .addAnswer(Answer("Übermorgen"))
        .addAnswer(Answer("In 43 Jahren"))
        .addAnswer(Answer("In ferner Zukunft")),
    Question(tag: "german_wessenthalben_question")
        .addRule(ContainsQuestionWord("wessenthalben"))
        .addAnswer(Answer("Dessenthalben")),
    Question(tag: "german_wie_viel_question")
        .addRule(ContainsQuestionWordSeries(["wie", "viel"]))
        .addAnswer(Answer("Garnichts"))
        .addAnswer(Answer("500"))
        .addAnswer(Answer("100000"))
        .addAnswer(Answer("1 Million"))
        .addAnswer(Answer("42"))
        .addAnswer(Answer("72"))
        .addAnswer(Answer("99"))
        .addAnswer(Answer("0", pronunciacion: "Null"))
        .addAnswer(Answer("69")),
    Question(tag: "german_warum_question")
        .addRule(OrRule([
          ContainsQuestionWord("weshalb"),
          ContainsQuestionWord("wieso"),
          ContainsQuestionWord("warum"),
          ContainsQuestionWord("weswegen"),
        ]))
        .addAnswer(Answer("Deswegen"))
        .addAnswer(Answer("Darum"))
        .addAnswer(Answer("Keine Ahnung")),
    Question(tag: "german_warum_question")
        .addRule(OrRule([
          ContainsQuestionWord("weshalb"),
          ContainsQuestionWord("wieso"),
          ContainsQuestionWord("warum"),
          ContainsQuestionWord("weswegen"),
        ]))
        .addAnswer(Answer("Deswegen"))
        .addAnswer(Answer("Darum"))
        .addAnswer(Answer("Keine Ahnung")),
    Question(tag: "german_wofuer_question")
        .addRule(OrRule([
          ContainsQuestionWord("wofür"),
          ContainsQuestionWord("wozu"),
        ]))
        .addAnswer(Answer("Deswegen"))
        .addAnswer(Answer("Darum"))
        .addAnswer(Answer("Keine Ahnung"))
  ];

  final finalDefaultAnswers = Question(tag: "german_ja_nein_question")
      .addAnswer(Answer("Ja", randomWeight: 10))
      .addAnswer(Answer("Nein", randomWeight: 10))
      .addAnswer(Answer("Jap"))
      .addAnswer(Answer("Klar"))
      .addAnswer(Answer("Nö"))
      .addAnswer(Answer("Eventuell"))
      .addAnswer(Answer("Womöglich"))
      .addAnswer(Answer("Möglicherweise"))
      .addAnswer(Answer("Ich weiß es nicht", randomWeight: 0.4))
      .addAnswer(Answer("Keineswegs"))
      .addAnswer(Answer("Nein wirklich nicht"))
      .addAnswer(Answer("Auf gar keinen Fall"))
      .addAnswer(Answer("Keine Ahnung"))
      .addAnswer(Answer("Gewiss"))
      .addAnswer(Answer("Keineswegs"))
      .addAnswer(Answer("Auf jeden Fall"))
      .addAnswer(Answer("Warum nicht"))
      .addAnswer(Answer("Vielleicht"));
  @override
  List<Question> get ruleSet => finalRuleSet;
  @override
  Question get defaultAnswers => finalDefaultAnswers;
}
