import 'package:flutter/widgets.dart';
import 'package:magic_conche/background/stars/star.dart';

class Stars extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double w = width(context);
    double h = height(context);

    var sizes = [0.3, 0.23, 0.21, 0.32, 0.2, 0.23]
        .map((size) => size * w * 1.3)
        .toList();

    return SizedBox(
      width: w,
      height: h,
      child: Stack(children: [
        Star(
          0,
          size: sizes[0],
          offset: Offset(-sizes[1] * 0.2, sizes[1] * 0.05),
        ),
        Star(
          1,
          size: sizes[1],
          offset: Offset(w - sizes[1] * 0.6, sizes[1] * 0.3),
        ),
        Star(
          2,
          size: sizes[2],
          offset: Offset(-20, h / 2 - sizes[2] * 0.45),
        ),
        Star(
          3,
          size: sizes[3],
          offset: Offset(w / 2 - sizes[3] * 0.1, h / 2 - sizes[3] * 0.3),
        ),
        Star(
          4,
          size: sizes[4],
          offset: Offset(sizes[4] * 0.1, h - h * 0.3),
        ),
        Star(
          5,
          size: sizes[5],
          offset: Offset(w - 1.1 * sizes[5], h - h * 0.28),
        ),
      ]),
    );
  }

  double width(BuildContext context) {
    return MediaQuery.of(context).size.width;
  }

  double height(BuildContext context) {
    return MediaQuery.of(context).size.height;
  }
}
