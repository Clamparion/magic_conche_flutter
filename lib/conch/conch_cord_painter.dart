import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:magic_conche/conch_colors.dart';
import 'package:vector_math/vector_math.dart';

class ConchCordPainter extends CustomPainter {
  final Vector2 pos;
  final bool isDragging;
  Vector2 initVector = Vector2(2, 6);
  final Vector2 anchor;
  factory ConchCordPainter(Vector2 anchor) {
    return new ConchCordPainter._(anchor, pos: Vector2(anchor.x, anchor.y));
  }

  ConchCordPainter._(this.anchor, {this.pos, this.isDragging = false});
  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = 4.0
      ..strokeCap = StrokeCap.round
      ..color = ConchColors.CordColor;

    final paint2 = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = 5.0
      ..strokeCap = StrokeCap.round
      ..color = ConchColors.CordBorderColor;

    final circlePaint = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = 4.0
      ..color = ConchColors.CordColor;

    final circleBorderPaint = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = 5.0
      ..color = ConchColors.CordBorderColor;

    var ringPos = pos;
    var directionVector = (ringPos - anchor);
    var len = directionVector.length;
    if (len < 6) {
      ringPos = anchor + initVector;
      directionVector = (ringPos - anchor);
      len = directionVector.length;
    }

    final dragPos = Offset(ringPos.x, ringPos.y);
    final anchorPos = Offset(anchor.x, anchor.y);

    final normalizedDirVector = len > 0 ? directionVector / len : directionVector;
    final newVec = anchor + (normalizedDirVector * (len + 6));

    canvas.drawLine(anchorPos, dragPos, paint2);
    canvas.drawLine(anchorPos, dragPos, paint);

    canvas.drawCircle(Offset(newVec.x, newVec.y), 6, circleBorderPaint);
    canvas.drawCircle(Offset(newVec.x, newVec.y), 6, circlePaint);
  }

  @override
  bool shouldRepaint(ConchCordPainter oldDelegate) {
    return isDragging != oldDelegate.isDragging || (oldDelegate.pos != pos);
  }

  copyWith({Vector2 newPos, bool newIsDragging}) {
    return new ConchCordPainter._(anchor,
        pos: newPos != null ? newPos : pos,
        isDragging: newIsDragging != null ? newIsDragging : isDragging);
  }
}
