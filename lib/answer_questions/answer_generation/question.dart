import 'package:magic_conche/answer_questions/answer_generation/answer.dart';
import 'package:magic_conche/answer_questions/answer_generation/question_rule.dart';
import 'package:dart_random_choice/dart_random_choice.dart';
import "dart:math";

class Question {
  List<QuestionRule> rules = List();
  List<Answer> answers = List();
  final rnd = Random();
  final String tag;

  Question({this.tag});

  Question addRule(QuestionRule rule) {
    rules.add(rule);
    return this;
  }

  Question addAnswer(Answer answer) {
    answers.add(answer);
    return this;
  }

  FullFillmentResult fullfillsAllRules(Iterable<String> question) {
    final allWeights = rules
        .map((rule) => rule.isFullfilledBy(question))
        .where((fullFillment) => fullFillment.isFullfilled)
        .map((fullFillment) => fullFillment.weight);

    if (allWeights.length == rules.length && allWeights.length > 0) {
      final avgWeight =
          (allWeights.reduce((value, element) => value + element) /
                  allWeights.length)
              .floor();
      return FullFillmentResult(true, avgWeight);
    } else {
      return FullFillmentResult(false, 0);
    }
  }

  Answer get randomAnswer {
    return randomChoice<Answer>(
        answers, answers.map((answer) => answer.randomWeight).toList());
  }
}
