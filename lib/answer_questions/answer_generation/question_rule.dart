import 'dart:math';

abstract class QuestionRule {
  FullFillmentResult isFullfilledBy(Iterable<String> question);
}

class FirstWordIs implements QuestionRule {
  final String word;

  factory FirstWordIs(String word) => FirstWordIs._(word.toLowerCase());
  FirstWordIs._(this.word);

  @override
  FullFillmentResult isFullfilledBy(Iterable<String> question) {
    return FullFillmentResult.fromQuestion(
        question.take(1).toList(), this.word);
  }
}

class OneOfTheFirstThreeWordsIs implements QuestionRule {
  final String word;

  factory OneOfTheFirstThreeWordsIs(String word) =>
      OneOfTheFirstThreeWordsIs._(word.toLowerCase());
  OneOfTheFirstThreeWordsIs._(this.word);

  @override
  FullFillmentResult isFullfilledBy(Iterable<String> question) {
    return FullFillmentResult.fromQuestion(
        question.take(3).toList(), this.word);
  }
}

class OrRule implements QuestionRule {
  final List<QuestionRule> questionRules;
  OrRule(this.questionRules);
  @override
  FullFillmentResult isFullfilledBy(Iterable<String> question) {
    final fullfilledWeights = this
        .questionRules
        .map((rule) => rule.isFullfilledBy(question))
        .where((fullFillment) => fullFillment.isFullfilled)
        .map((fullFillment) => fullFillment.weight)
        .toList();

    if (fullfilledWeights.length > 0) {
      final avgWeight =
          (fullfilledWeights.reduce((value, element) => value + element) /
                  fullfilledWeights.length)
              .floor();
      return FullFillmentResult(true, avgWeight);
    } else {
      return FullFillmentResult(false, 0);
    }
  }
}

class ContainsQuestionWord implements QuestionRule {
  final String word;

  factory ContainsQuestionWord(String word) =>
      ContainsQuestionWord._(word.toLowerCase());
  ContainsQuestionWord._(this.word);

  @override
  FullFillmentResult isFullfilledBy(Iterable<String> question) {
    final searchQuestion = question.take(min(2, question.length)).toList();
    return FullFillmentResult.fromQuestion(searchQuestion, this.word);
  }
}

class ContainsQuestionWordSeries implements QuestionRule {
  final List<String> words;

  factory ContainsQuestionWordSeries(List<String> words) =>
      ContainsQuestionWordSeries._(
          words.map((word) => word.toLowerCase()).toList());
  ContainsQuestionWordSeries._(this.words);

  @override
  FullFillmentResult isFullfilledBy(Iterable<String> question) {
    if (words.length == 0) {
      return FullFillmentResult(false, 0);
    }

    final questionList = question.toList();
    final firstWordIndex = questionList
        .take(min(2, question.length))
        .toList()
        .indexOf(this.words[0]);
    final List<int> indezes = [firstWordIndex];
    if (firstWordIndex >= 0) {
      for (var i = 1; i < this.words.length; i++) {
        if (firstWordIndex + i < questionList.length &&
            questionList[firstWordIndex + i] == this.words[i]) {
          indezes.add(firstWordIndex + i);
        }
      }
    }
    if (indezes.length == words.length) {
      final totalWeight = indezes
          .map((index) =>
              FullFillmentResult.weightFromIndex(index, question.length))
          .reduce((value, element) => value + element);
      return FullFillmentResult(true, totalWeight);
    }
    return FullFillmentResult(false, 0);
  }
}

class FullFillmentResult {
  final bool isFullfilled;
  final int weight;

  factory FullFillmentResult.fromQuestion(List<String> question, word) {
    final index = question.indexOf(word);
    return FullFillmentResult(
        index >= 0, weightFromIndex(index, question.length));
  }

  FullFillmentResult(this.isFullfilled, this.weight);

  static int weightFromIndex(int index, int length) {
    return index < 0 ? 0 : length - index;
  }
}
