import 'package:flutter/widgets.dart';

import '../../conch_colors.dart';
import 'hill.dart';
import 'hill_1_clipper.dart';
import 'hill_2_clipper.dart';
import 'hill_3_clipper.dart';

class Hills extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double h = height(context);

    return Stack(children: [
      Hill(h * 0.17, Hill3Clipper(),
          gradientStartColor: ConchColors.hill3GradientStartColor,
          gradientEndColor: ConchColors.hill3GradientEndColor),
      Hill(h * 0.2, Hill2Clipper(),
          gradientStartColor: ConchColors.hill2GradientStartColor,
          gradientEndColor: ConchColors.hill2GradientEndColor),
      Hill(h * 0.1, Hill1Clipper(),
          gradientStartColor: ConchColors.hill1GradientStartColor,
          gradientEndColor: ConchColors.hill1GradientEndColor)
    ]);
  }

  double height(BuildContext context) {
    return MediaQuery.of(context).size.height;
  }
}
