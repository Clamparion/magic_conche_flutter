import 'dart:ui';

import 'package:flutter/widgets.dart';
import 'package:magic_conche/background/rays/ray.dart';

class Rays extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var w = width(context);
    var h = height(context);

    return Stack(
      children: [
        Ray(
          Size(w * 0.2, h * 0.8),
          angle: -0.2,
          origin: Offset(w * 0.7, -h * 0.1),
          opacity: 0.2,
        ),
        Ray(
          Size(w * 0.2, h * 0.8),
          angle: -0.1,
          origin: Offset(w * 0.6, -h * 0.1),
          opacity: 0.4,
        ),
        Ray(
          Size(w * 0.2, h * 0.8),
          angle: 0,
          origin: Offset(w / 2, -h * 0.1),
          opacity: 0.6,
        ),
        Ray(
          Size(w * 0.2, h * 0.8),
          angle: 0.1,
          origin: Offset(w * 0.4, -h * 0.1),
          opacity: 0.4,
        ),
        Ray(
          Size(w * 0.2, h * 0.8),
          angle: 0.2,
          origin: Offset(w * 0.3, -h * 0.1),
          opacity: 0.3,
        ),
      ],
    );
  }

  double width(BuildContext context) {
    return MediaQuery.of(context).size.width;
  }

  double height(BuildContext context) {
    return MediaQuery.of(context).size.height;
  }
}
